class Users::RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters

  def create
    super
  end

  def destroy
    resource.soft_delete
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    set_flash_message :notice, :destroyed if is_navigational_format?
    respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).push(:first_name, :last_name, :picture_url, :birth_date, :sex, :street_address, :city, :state, :zip, :contact_phone)
    devise_parameter_sanitizer.for(:account_update).push(:first_name, :last_name, :picture_url, :birth_date, :sex, :street_address, :city, :state, :zip, :contact_phone)
   end
end
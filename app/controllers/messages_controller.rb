class MessagesController < ApplicationController

  # GET /message/new
  def new
    @message = current_user.messages.new
  end

   # POST /message/create
  def create
    #binding.pry
    @recipient = User.find(params[:mailboxer_message][:recipients])
    current_user.send_message(@recipient, params[:mailboxer_message][:body], params[:mailboxer_message][:subject])
    flash[:notice] = "Message has been sent!"
    redirect_to :conversations
  end

  def index
    redirect_to new_message_path
  end

end



#class MessagesController < ApplicationController
#
#  require 'pry'
#  before_filter :authenticate_user!
#  before_filter :get_mailbox, :get_box, :get_user
#
#
#  def index
#    redirect_to conversations_path(:box => @box)
#  end
#
#  # GET /messages/1
#  # GET /messages/1.xml
#  def show
#    #if @message = Message.find_by_id(params[:id]) and @conversation = @message.conversation
#      if @conversation.is_participant?(@user)
#        redirect_to conversation_path(@conversation, :box => @box, :anchor => "message_" + @message.id.to_s)
#      return
#      end
#    #end
#    redirect_to conversations_path(:box => @box)
#  end
#
#  # GET /messages/new
#  # GET /messages/new.xml
#  def new
#    if params[:receiver].present?
#      @recipient = User.find_by_slug(params[:receiver])
#      return if @recipient.nil?
#      @recipient = nil if User.normalize(@recipient)==User.normalize(current_subject)
#    end
#  end
#
#  # GET /messages/1/edit
#  def edit
#
#  end
#
#  # POST /messages
#  # POST /messages.xml
#  def create
#    @recipients =
#      if params[:message][:recipients].present?
#        @recipients = params[:message][:recipients].split(',').map{ |r| User.find_by_email(r) }
#      else
#        []
#      end
#    #binding.pry
#    @receipt = @user.send_message(@recipients, params[:message][:body], params[:message][:subject])
#    if (@receipt.errors.blank?)
#      @conversation = @receipt.conversation
#      flash[:success]= ('mailboxer.sent')
#      redirect_to conversation_path(@conversation, :box => :sentbox)
#    else
#      render :action => :new
#    end
#  end
#
#  # PUT /messages/1
#  # PUT /messages/1.xml
#  def update
#
#  end
#
#  # DELETE /messages/1
#  # DELETE /messages/1.xml
#  def destroy
#
#  end
#
#  private
#
#  def get_mailbox
#    @mailbox ||= current_user.mailbox
#  end
#
#  def get_user
#    @user ||= current_user
#  end
#
#  def get_box
#    if params[:box].blank? or !["inbox","sentbox","trash"].include?params[:box]
#      @box = "inbox"
#    return
#    end
#    @box = params[:box]
#  end
#
#end#
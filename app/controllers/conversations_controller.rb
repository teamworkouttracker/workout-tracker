class ConversationsController < ApplicationController
  before_filter :authenticate_user!
  #before_filter :mailbox, :get_box
  before_filter :get_mailbox, :get_box
  #helper_method :mailbox, :conversation
  #before_filter :check_current_subject_in_conversation, :only => [:show, :update, :destroy]
  require 'pry'

  def index
    if @box.eql? "inbox"
      @conversations = @mailbox.inbox.page(params[:page]).per(9)
    elsif @box.eql? "sentbox"
      @conversations = @mailbox.sentbox.page(params[:page]).per(9)
    else
      @conversations ||= @mailbox.trash.page(params[:page]).per(9)
    end

    respond_to do |format|
      format.html { render @conversations if request.xhr? }
    end
  end

  def show
    if @box.eql? 'trash'
      @receipts = @mailbox.receipts_for(conversation).trash
    else
      @receipts = @mailbox.receipts_for(conversation).not_trash
    end
    @receipts.mark_as_read
  end

  def create
    recipient_emails = conversation_params(:recipients).split(',')
    recipients = User.where(email: recipient_emails).all

    conversation = current_user.send_message(recipients, *conversation_params(:body, :subject)).conversation

    redirect_to conversation
  end

  def reply
    current_user.reply_to_conversation(conversation, *conversation_params(:body))
    redirect_to conversation_path
  end

  def trashbin
    @trash ||= current_user.mailbox.trash.all
  end

  def trash
    conversation.move_to_trash(current_user)
    redirect_to :conversations
  end

  def empty_trash
    current_user.mark_as_deleted(mailbox.trash.to_a)
    redirect_to root
  end

  private

  def get_mailbox
   @mailbox ||= current_user.mailbox
  end

  def conversation
   @conversation ||= @mailbox.conversations.find(params[:id])
  end

  def get_box
    if params[:box].blank? or !["inbox","sentbox","trash"].include?params[:box]
      params[:box] = 'inbox'
    end

    @box = params[:box]
  end

  def check_current_subject_in_conversation
    @conversation = Conversation.find_by_id(params[:id])

    if @conversation.nil? or !@conversation.is_participant?(@actor)
      redirect_to conversations_path(:box => @box)
    return
    end
  end

  def conversation_params(*keys)
   fetch_params(:conversation, *keys)
  end

  def message_params(*keys)
   fetch_params(:message, *keys)
  end

  def fetch_params(key, *subkeys)
   params[key].instance_eval do
     case subkeys.size
     when 0 then self
     when 1 then self[subkeys.first]
     else subkeys.map{|k| self[k] }
     end
   end
  end

end



module ApplicationHelper

  def show_unread_count
      @messages_count = current_user.mailbox.inbox({:read => false}).count
  end

  def format_datetime(datetime,type=:long)
    if type == :short && Time.now - 0.5.days < datetime
      distance_of_time_in_words(Time.now, datetime) + ' ago'
    else
      datetime.strftime("%A, %b %e, %Y at %l:%M%p")
    end
  end

  def get_sender_picture_url(user_id)
    User.find(user_id).picture_url
  end

  def activity_fields_removal_link(form)
    activity = form.object
    #unless activity.workout.present?
      link = link_to_remove_fields('Remove Activity', form).html_safe
    #end
  end

  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to('<i style="color:red;margin-left:-35px" class="fa fa-2x fa-minus-square"></i>'.html_safe, '#', class: 'remove-fields') 
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to '<i style="color:orange" class="fa fa-3x fa-plus-square"></i>'.html_safe, '#',
    class: 'add-fields',
      data: {
        association: association,
        fields: "#{fields}"
      }
    end

  end

class Workout < ActiveRecord::Base
    has_many :activities, inverse_of: :workout, dependent: :destroy
    has_many :exercises, :through => :activities
    accepts_nested_attributes_for :activities,
      reject_if: proc { |a| a[:sets].blank?; },
      :allow_destroy => true

    belongs_to :user, inverse_of: :workouts

    validates_presence_of :title
    validates_presence_of :user

    validates_presence_of :activities
end

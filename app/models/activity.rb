class Activity < ActiveRecord::Base
  belongs_to :exercise
  belongs_to :workout, inverse_of: :activities

  validates_presence_of :sets
  validates_presence_of :reps
  validates_presence_of :workout
  validates_presence_of :exercise

  validates_numericality_of :sets, only_integer: true, greater_than_or_equal_to: 1
  validates_numericality_of :reps, only_integer: true, greater_than_or_equal_to: 1
  validates_numericality_of :weight, if: "!weight.nil?"

  validates_inclusion_of :sets, :in => 1..99, :message => " should be a number between 1 and 99"
  validates_inclusion_of :reps, :in => 1..1000, :message => " should be a number between 1 and 1000"
  validates_inclusion_of :weight, :in => 1..1000, :message => " should be a number between 1 and 1000", if: "!weight.nil?"

end

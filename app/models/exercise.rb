class Exercise < ActiveRecord::Base
    has_many :activities, inverse_of: :exercise
    has_many :workouts, :through => :activities

    belongs_to :user

    validates_presence_of :title
    validates_presence_of :user

    validates_uniqueness_of :title, :scope => [:user], :case_sensitive => false, :message => "already exists"

    before_destroy :check_exercise_not_in_use

    def check_exercise_not_in_use
      self.activities.empty?
    end

end

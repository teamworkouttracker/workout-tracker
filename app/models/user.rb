class User < ActiveRecord::Base
  enum role: [:user, :trainer, :admin]
  after_initialize :set_default_role, :if => :new_record?
  acts_as_messageable

  def set_default_role
    self.role ||= :user
  end

  def soft_delete
    # assuming you have deleted_at column added already
    update_attribute(:deleted_at, Time.current)
  end

  def name
    return "#{first_name} #{last_name}"
  end

  def mailboxer_email(object)
   return "#{email}"
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :exercises
  has_many :workouts

end

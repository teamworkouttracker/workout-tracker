# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#options are inline and popup
$.fn.editable.defaults.mode = 'inline'
$ ->
  $("[data-xeditable=true]").each ->
    $(@).editable
      ajaxOptions:
        type: "PUT"
        dataType: "json"
      params: (params) ->
        railsParams = {}
        railsParams[$(@).data("model")] = {}
        railsParams[$(@).data("model")][params.name] = params.value

        return railsParams

remove_fields = (link) ->
  $(link).prev("input[type=hidden]").val "1"
  $(link).closest(".fields").hide()
  return

add_fields = (link, association, content) ->
  new_id = new Date().getTime()
  regexp = new RegExp("new_" + association, "g")
  $(link).parent().parent().before content.replace(regexp, new_id)
  return

$(document).ready ->
  $("#new_workout").on "click", ".add-fields", (e) ->
    e.preventDefault()
    link = this
    association = $(this).data("association")
    fields = $(this).data("fields")
    add_fields link, association, fields
    return

  $("#new_workout").on "click", ".remove-fields", (e) ->
    e.preventDefault()
    link = this
    remove_fields link
    return

  $(".edit_workout").on "click", ".add-fields", (e) ->
    e.preventDefault()
    link = this
    association = $(this).data("association")
    fields = $(this).data("fields")
    add_fields link, association, fields
    return

  $(".edit_workout").on "click", ".remove-fields", (e) ->
    e.preventDefault()
    link = this
    remove_fields link
    return
return

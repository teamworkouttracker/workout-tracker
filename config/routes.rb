Rails.application.routes.draw do
	root to: 'workouts#index'
	devise_for :users, skip: :registrations
	devise_scope :user do
		resource :registration,
		only: [:new, :create, :edit, :update, :destroy],
		path: 'users',
		path_names: { new: 'sign_up' },
		controller: 'users/registrations',
		as: :user_registration do
			get :cancel
		end
	end

	#Main Controllers
	resources :users
	resources :profiles
	resources :dashboard
	resources :workouts
    resources :exercises
    resources :activities

	#temporary while contructing pages.
	resources :not_implemented

resources :messages do
  member do
    post :new
  end
end
resources :conversations, only: [:index, :show, :new, :create] do
  member do
    post :reply
    post :trash
    post :untrash
    post :empty_trash
  end
 collection do
    get :trashbin
    get :empty_trash
 end
end

end
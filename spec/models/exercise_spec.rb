require 'rails_helper'

feature 'exercises CRUD', %Q{
  As an authenticated user
  I want to add exercises
  So that I change what exercises I have available to me
} do

    # ACCEPTANCE CRITERIA
    # * I can create an exercise with a title and description when logged in
    # * The title of the exercise must be unique
    # * I can see a list of all my exercise
    # * I cannot delete an exercise that is part of a workout

    let(:user) { FactoryGirl.create(:user) }

    def fill_in_form_and_submit(form_title, form_desc)
        fill_in 'exercise_title', with: form_title
        fill_in 'exercise_description', with: form_desc
        click_button 'Create Exercise'
    end

    scenario 'Creates Exercise with valid information' do
        signin(user.email, user.password)
        prev_count = Exercise.count
        visit new_exercise_path
        fill_in 'exercise_title', with: 'New Test Title'
        fill_in 'exercise_description', with: 'Exercise Description'
        click_button 'Create Exercise'
        expect(page).to have_content("Exercise was successfully created.")
        expect(Exercise.count).to eql(prev_count + 1)
    end

    scenario 'Remove Exercise' do
        Capybara.current_driver = :selenium_firefox

        visit '/users/sign_up'
        fill_in 'user_email', with: user.email
        fill_in 'user_password', with: user.password
        fill_in 'user_password_confirmation', with: user.password
        click 'Register'

        prev_count = Exercise.count
        visit new_exercise_path
        fill_in 'exercise_title', with: 'New Test Title'
        fill_in 'exercise_description', with: 'Exercise Description'
        click_button 'Create Exercise'
        expect(page).to have_content("Exercise was successfully created.")
        expect(Exercise.count).to eql(prev_count + 1)

        click_link 'Back'
        click_link 'Remove'
        a = page.driver.browser.switch_to.alert
        a.accept
        expect(page).to have_content('Exercise was successfully destroyed.')
        expect(Exercise.count).to eql(prev_count)
        Capybara.reset_current_driver
    end

    scenario 'Cannot create exercise without title' do
        signin(user.email, user.password)
        prev_count = Exercise.count
        visit new_exercise_path
        fill_in 'exercise_description', with: 'Exercise Description'
        click_button 'Create Exercise'
        #puts page.body
        expect(page).to have_content("can't be blank")
        expect(Exercise.count).to eql(prev_count)
    end

    scenario 'Cannot create duplicate exercise' do
        exercise = 'Test Exercise'
        signin(user.email, user.password)
        FactoryGirl.create(:exercise, title: exercise, user_id: user.id)
        prev_count = Exercise.count
        visit new_exercise_path
        fill_in 'exercise_title', with: exercise
        click_button 'Create Exercise'
        expect(page).to have_content("already exists")
        expect(Exercise.count).to eql(prev_count)
    end

    scenario 'User sees a list of multiple exercises' do
        exercise_title = 'Test Exercise #1'
        exercise2_title = 'Test Exercise #2'
        exercise_description = 'Test Description #1'
        exercise2_description = 'Test Description #2'

        signin(user.email, user.password)
        visit exercises_path
        prev_count = Exercise.count

        click_link 'New Exercise'
        fill_in_form_and_submit(exercise_title, exercise_description)
        expect(page).to have_content("Exercise was successfully created.")
        click_link 'Back'

        click_link 'New Exercise'
        fill_in_form_and_submit(exercise2_title, exercise2_description)
        expect(page).to have_content("Exercise was successfully created.")
        click_link 'Back'

        expect(page).to have_content(exercise_title)
        expect(page).to have_content(exercise_description)
        expect(page).to have_content(exercise2_title)
        expect(page).to have_content(exercise2_description)
        expect(Exercise.count).to eql(prev_count + 2)
    end




end
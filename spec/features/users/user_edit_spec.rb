include Warden::Test::Helpers
Warden.test_mode!
require 'pry'

# Feature: User edit
#   As a user
#   I want to edit my user profile
#   So I can change my email address
feature 'User edit', :devise do

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User changes email address
  #   Given I am signed in
  #   When I change my email address
  #   Then I see an account updated message
  scenario 'user changes email address' do
    user = FactoryGirl.create(:user)
    login_as(user, :scope => :user)
    @profile = Profile.new
    @profile.update(user_id: user.id)
    visit edit_user_registration_path(user)
    fill_in 'Email', :with => 'newemail@example.com'
    fill_in 'Current password', :with => user.password
    click_button 'Update'
    expect(page).to have_content 'You updated your account successfully.'
  end

  # Scenario: User cannot edit another user's profile
  #   Given I am signed in
  #   When I try to edit another user's profile
  #   Then I see my own 'edit profile' page
  scenario "user cannot cannot edit another user's profile", :me do
    me = FactoryGirl.create(:user)
    other = FactoryGirl.create(:user, email: 'other@example.com')
    login_as(me, :scope => :user)
    profile_me = Profile.new
    profile_me.update(user_id: me.id)
    profile_other = Profile.new
    profile_other.update(user_id: other.id)
    visit edit_user_registration_path(other)
    expect(page).to have_content 'Edit Account'
    expect(page).to have_field('Email', with: me.email)
  end

end

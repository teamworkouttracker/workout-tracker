# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :profile do
    picture_url "MyString"
    birth_date "MyString"
    sex "MyString"
    street_address "MyString"
    city "MyString"
    state "MyString"
    zip "MyString"
    contact_phone "MyString"
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl
FactoryGirl.define do

  factory :exercise do
    title "My Exercise Title"
    description "My Exercise Description"
    user
  end
end

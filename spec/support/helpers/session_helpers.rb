module Features
  module SessionHelpers 
    def sign_up_with(email, password, confirmation)
      visit new_user_registration_path
      fill_in 'Email', with: email
      fill_in 'Password', with: password
      fill_in 'Password confirmation', :with => confirmation
      click_button 'Register'
    end

    def signin(email, password)
      visit new_user_session_path
      fill_in 'Email', with: email
      fill_in 'Password', with: password
      click_button 'Sign In'
    end

    #def create_new_workout(workoutTitle, workoutDescription, notes, sets, reps, weight, duration)
    #  visit new_workout_path
    #end
  end
end
class AddFieldsToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :reps, :integer
    add_column :activities, :sets, :integer
    add_column :activities, :weight, :integer
    add_column :activities, :duration, :string
  end
end

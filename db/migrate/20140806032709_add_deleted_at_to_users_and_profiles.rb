class AddDeletedAtToUsersAndProfiles < ActiveRecord::Migration
  def change
    add_column :users, :deleted_at, :string
    add_column :profiles, :deleted_at, :string
  end
end

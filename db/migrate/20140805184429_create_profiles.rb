class CreateProfiles < ActiveRecord::Migration
  def change
    if !table_exists?("profiles")
      create_table :profiles do |t|
        t.belongs_to :users, index:true
        t.string :picture_url
        t.string :birth_date
        t.string :sex
        t.string :street_address
        t.string :city
        t.string :state
        t.string :zip
        t.string :contact_phone

        t.timestamps
      end
    end
  end
end

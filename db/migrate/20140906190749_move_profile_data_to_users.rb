class MoveProfileDataToUsers < ActiveRecord::Migration
  def change
    drop_table :profiles

    add_column :users, :picture_url, :string, :limit => 255
    add_column :users, :birth_date, :date
    add_column :users, :sex, :string, :limit => 255
    add_column :users, :street_address, :string, :limit => 255
    add_column :users, :city, :string, :limit => 255
    add_column :users, :state, :string, :limit => 255
    add_column :users, :zip, :integer
    add_column :users, :contact_phone, :string, :limit => 15

  end
end

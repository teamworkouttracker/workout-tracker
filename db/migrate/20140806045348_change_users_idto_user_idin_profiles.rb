class ChangeUsersIdtoUserIdinProfiles < ActiveRecord::Migration
  def change
  		remove_column(:profiles, :users_id) unless column_exists? :profiles, :user_id
  		add_column(:profiles, :user_id, :integer) unless column_exists? :profiles, :user_id
  end
end

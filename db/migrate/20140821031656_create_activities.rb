class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.references :exercise, index: true
      t.references :workout, index: true
      t.text :notes

      t.timestamps
    end

  end
end

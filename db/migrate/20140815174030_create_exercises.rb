class CreateExercises < ActiveRecord::Migration
  def change
    create_table :exercises do |t|
        t.string :title
        t.text :description
        t.integer :user_id

      t.timestamps
    end

    add_index :exercises, :user_id
  end
end

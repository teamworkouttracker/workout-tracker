# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email


exercise_list = [
    ["Chest Press", "On you back.", 2],
    ["Squat", "Do it carefully.", 2],
    ["Lunge", "With care.", 2]
]

exercise_list.each do |title, description, user_id|
    Exercise.create( title: title, description: description, user_id: user_id)
    puts 'Created Exercise ' << title
end